﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking
{
    internal enum CommandCode : byte
    {
        ControlCar = 1,
        VideoStream = 2,
        ControlRobot = 3,
        MapImage = 4
    }
}
