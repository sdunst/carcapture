﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking.SubCommands
{
    public class ControlRobotCmd : ICommand
    {
        internal byte CarNr { get; private set; }
        internal sbyte Speed { get; private set; }
        internal sbyte Direction { get; private set; }

        private ControlRobotCmd() { }

        public IEnumerable<sbyte> GetBytes()
        {
            List<sbyte> bytes = new List<sbyte>();
            bytes.Add((sbyte)CarNr);
            bytes.Add(Speed);
            bytes.Add(Direction);

            return bytes;
        }

        internal static ICommand FromNetworkData(sbyte[] receivedData)
        {
            ControlRobotCmd cmd = new ControlRobotCmd();
            cmd.CarNr = (byte)receivedData[1];
            cmd.Speed = receivedData[2];
            cmd.Direction = receivedData[3];

            return cmd;
        }

        internal static ICommand CreateControlCommand(byte carNr, sbyte speed, sbyte direction)
        {
            ControlRobotCmd cmd = new ControlRobotCmd();
            cmd.CarNr = carNr;
            cmd.Speed = speed;
            cmd.Direction = direction;

            return cmd;
        }
    }
}
