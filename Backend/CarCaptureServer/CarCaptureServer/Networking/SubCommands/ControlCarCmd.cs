﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking.SubCommands
{
    internal class ControlCarCmd : ICommand
    {
        internal enum ControlMethod : sbyte
        {
            Drive = 1,
            Shoot = 2
        }

        private ControlCarCmd() { }

        internal static ControlCarCmd FromNetworkData(sbyte[] receivedData)
        {
            ControlCarCmd cmd = new ControlCarCmd();
            cmd.Method = (ControlMethod)receivedData[1];
            if (cmd.Method == ControlMethod.Drive)
            {
                cmd.Speed = receivedData[2];
                cmd.Direction = receivedData[3];
            }
            return cmd;
        }

        internal static ControlCarCmd CreateShootCmd()
        {
            return new ControlCarCmd { Method = ControlMethod.Shoot };
        }

        internal static ControlCarCmd CreateDriveCmd(sbyte speed, sbyte direction)
        {
            return new ControlCarCmd { Method = ControlMethod.Drive, Direction = direction, Speed = speed };
        }

        internal ControlMethod Method { get; private set; }
        internal sbyte Speed { get; private set; }
        internal sbyte Direction { get; private set; }

        public IEnumerable<sbyte> GetBytes()
        {
            List<sbyte> bytes = new List<sbyte>();
            bytes.Add((sbyte)Method);
            if (Method == ControlMethod.Drive)
            {
                bytes.Add(Speed);
                bytes.Add(Direction);
            }

            return bytes;
        }
    }
}
