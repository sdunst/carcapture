﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking.SubCommands
{
    internal class MapImageCmd : ImageCmd
    {
        private MapImageCmd() { }

        internal static ICommand FromNetworkData(sbyte[] receivedData)
        {
            return ImageCmd.FromNetworkData(receivedData, new MapImageCmd());
        }
    }
}
