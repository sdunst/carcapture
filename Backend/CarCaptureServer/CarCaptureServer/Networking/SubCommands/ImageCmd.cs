﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking.SubCommands
{
    internal abstract class ImageCmd : ICommand
    {
        internal Bitmap MapImage { get; private set; }

        public IEnumerable<sbyte> GetBytes()
        {
            IEnumerable<sbyte> bytes = null;
            using (MemoryStream stream = new MemoryStream())
            {
                MapImage.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Close();

                bytes = stream.ToArray().Select(b => (sbyte)b);
            }
            return bytes;
        }

        internal static ICommand FromNetworkData(sbyte[] receivedData, ImageCmd cmd)
        {
            using (MemoryStream stream = new MemoryStream(receivedData.Skip(1).Select(b => (byte)b).ToArray()))
            {
                cmd.MapImage = new Bitmap(stream);
            }
            return cmd;
        }
    }
}
