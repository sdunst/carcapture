﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking.SubCommands
{
    class ShootResultCmd : ICommand
    {
        public sbyte Result { get; private set; }

        private ShootResultCmd()
        {
        }

        internal static ICommand CreateShootResultCommand(bool success)
        {
            ShootResultCmd cmd = new ShootResultCmd();
            cmd.Result = success ? (sbyte)1 : (sbyte)0;
            return cmd;
        }

        public IEnumerable<sbyte> GetBytes()
        {
            var res = new List<sbyte>();
            res.Add(Result);
            return res;
        }
    }
}
