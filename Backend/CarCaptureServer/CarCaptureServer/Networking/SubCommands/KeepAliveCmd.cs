﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking.SubCommands
{
    class KeepAliveCmd : ICommand
    {
        private KeepAliveCmd()
        {
        }

        internal static KeepAliveCmd CreateNew()
        {
            return new KeepAliveCmd();
        }

        public IEnumerable<sbyte> GetBytes()
        {
            List<sbyte> res = new List<sbyte>();
            res.Add((sbyte)-127);
            return res;
        }
    }
}
