﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking.SubCommands
{
    internal class VideoStreamCmd : ImageCmd
    {
        private VideoStreamCmd() { }

        internal static ICommand FromNetworkData(sbyte[] receivedData)
        {
            return ImageCmd.FromNetworkData(receivedData, new VideoStreamCmd());
        }
    }
}
