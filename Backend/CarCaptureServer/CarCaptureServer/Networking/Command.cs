﻿using CarCaptureServer.Networking.SubCommands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking
{
    internal class Command : ICommand
    {
        public CommandCode Code { get; private set; }
        public ICommand SubCommand { get; private set; }

        private Command()
        {
        }

        internal static Command FromNetworkData(sbyte[] receivedData)
        {
            Command cmd = new Command();
            cmd.Code = (CommandCode)receivedData[0];

            switch (cmd.Code)
            {
                case CommandCode.ControlCar:
                    cmd.SubCommand = ControlCarCmd.FromNetworkData(receivedData);
                    break;
                case CommandCode.ControlRobot:
                    cmd.SubCommand = ControlRobotCmd.FromNetworkData(receivedData);
                    break;
                case CommandCode.MapImage:
                    cmd.SubCommand = MapImageCmd.FromNetworkData(receivedData);
                    break;
                case CommandCode.VideoStream:
                    cmd.SubCommand = VideoStreamCmd.FromNetworkData(receivedData);
                    break;
                default:
                    throw new ArgumentException(string.Format("Unknown Command code: {0}", cmd.Code));
            }

            return cmd;
        }

        internal static Command CreateNew(CommandCode code, ICommand subCommand)
        {
            Command cmd = new Command();
            cmd.Code = code;
            cmd.SubCommand = subCommand;

            return cmd;
        }

        public IEnumerable<sbyte> GetBytes()
        {
            List<sbyte> bytes = new List<sbyte>();
            bytes.Add((sbyte)Code);
            bytes.AddRange(SubCommand.GetBytes());

            return bytes;
        }
    }
}
