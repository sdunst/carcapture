﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking
{
    interface ICommand
    {
        IEnumerable<sbyte> GetBytes();
    }
}
