﻿using CarCaptureServer.CarControl;
using CarCaptureServer.ImageProcessing;
using CarCaptureServer.Networking.SubCommands;
using CarCaptureServer.VideoStream;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CarCaptureServer.Networking
{
    internal class UDPServer : IDisposable
    {
        private const int COM_PORT = 4711;

        private readonly UdpClient server;
        private IPEndPoint endPoint;
        private readonly ThreadStart exec;

        private bool running;

        private CarController carController;
        private PositionDetection positionDetection;
        private VideoStreamer videoStreamer;

        private IDictionary<IPAddress, UdpClient> clients;

        internal UDPServer(CarController carController, PositionDetection positionDetection, VideoStreamer videoStreamer)
        {
            this.carController = carController;
            this.positionDetection = positionDetection;
            this.videoStreamer = videoStreamer;
            clients = new Dictionary<IPAddress, UdpClient>();

            this.running = false;

            this.endPoint = new IPEndPoint(IPAddress.Any, COM_PORT);
            this.server = new UdpClient(endPoint);
            server.Client.ReceiveBufferSize = 512;

            this.exec = new ThreadStart(Run);
        }

        internal void Start()
        {
            this.running = true;
            new Thread(exec).Start();
        }

        public void Stop()
        {
            this.running = false;
        }

        private void Run()
        {
            while (running)
            {
                sbyte[] buffer = server.Receive(ref endPoint).Select(b => (sbyte)b).ToArray();
                Command cmd = Command.FromNetworkData(buffer);

                if (cmd != null)
                {
                    switch (cmd.Code)
                    {
                        case CommandCode.ControlCar:
                            carController.SendCommand((ControlCarCmd)cmd.SubCommand, endPoint.Address);
                            break;
                        case CommandCode.MapImage:
                            //positionDetection.ProcessImage((MapImageCmd)cmd.SubCommand);
                            break;
                        case CommandCode.VideoStream:
                            videoStreamer.ForwardImage((VideoStreamCmd)cmd.SubCommand);
                            break;
                        case CommandCode.ControlRobot:
                            //Should not happen
                            break;
                    }
                }
                else
                {
                    //Dafuq?
                }
            }
        }

        internal void SendMessage(ICommand cmd, IPAddress target)
        {
            if (!clients.ContainsKey(target))
            {
                UdpClient cached = new UdpClient();
                cached.Connect(new IPEndPoint(target, COM_PORT));
                cached.Client.SendBufferSize = 512;

                clients.Add(target, cached);
            }

            IEnumerable<sbyte> toSend = cmd.GetBytes();
            clients[target].Send(toSend.Select(b => (byte)b).ToArray(), toSend.Count());
        }

        public void Dispose()
        {
            if (this.server != null)
            {
                this.server.Close();
            }

            if (this.clients != null)
            {
                foreach (var client in clients)
                {
                    client.Value.Close();
                }
            }
        }
    }
}
