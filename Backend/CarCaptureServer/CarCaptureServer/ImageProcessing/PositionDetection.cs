﻿using CarCaptureServer.Networking.SubCommands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Emgu.CV;
using Emgu.CV.Cvb;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.Features2D;
using Emgu.CV.Util;
using Emgu.CV.VideoSurveillance;
using System.Threading;

namespace CarCaptureServer.ImageProcessing
{
    internal class PositionDetection
    {
        // TODO: calibration.cpp in openCV tutorial: kalibrierung der kamera, weil an den rändern verzerrt

        private static int MIN_OBJ_SIZE = 3;
        private static int MAX_CAR_SIZE = 100;

        private static int MIN_OBJ_COUNT = 2; 

        private static int MAX_MEDIAN_VELOCITIES = 5;

        private static int SHOW_MESSAGE_TIME = 3000;

        private static int PLAYING_TIME = 60000;

        private static double MAX_ACCURACY = 0.1;
        
        private Bgr bgrLowAiCar = new Bgr(227, 227, 227);
        private Bgr bgrHighAiCar = new Bgr(255, 255, 255);

        private Bgr bgrLowPlayerCar = new Bgr(132, 154, 210);
        private Bgr bgrHighPlayerCar = new Bgr(222, 244, 300);

        private int edgeCount = 4;
        private int minArea = 150;
        private int angleTolerance = 10;
        private int accuarcy = 100;
        private int cannyThresh = 50;
        private int cannyLinking = 120;

        private int timeLeft = PLAYING_TIME;

        // DETECTION BY MANUAL BLOB-DETECTION ///////////////////////////////// 
        private calibrationLabel window;
        private IFrameObject calibObj;
        private int selectedId;

        private IFrameObject playerCar = null;
        private IFrameObject aiCar = null;

        private bool calibratedFlag = false;
        private bool runGame = false;

        private String messageString = null;
        private long messageTime = 0;
        private BackgroundSubstractor subtractor;
        //////////////////////////////////////////////////////////////////////

        // DETECTION BY SHAPES ///////////////////////////////// 
        private MemStorage storage = new MemStorage();

        private Gray cannyThreshold = new Gray(50);
        private Gray cannyThresholdLinking = new Gray(200);

        private Dictionary<int, Tuple<int, int>> angleRanges = new Dictionary<int, Tuple<int, int>>();
        private Dictionary<int, Contour<Point>> detectedContoursInFrame = new Dictionary<int, Contour<Point>>();
        private List<Contour<Point>> triangleConturs = new List<Contour<Point>>();
        private Dictionary<int, IFrameObject> stableKnownObjects = new Dictionary<int, IFrameObject>();

        private const long OBJECT_LOST_DURATION = 10;
        private const long OBJECT_DETECTED_DURATION = 10;
        //////////////////////////////////////////////////////////////////////

        // DETECTION BY COLOR ///////////////////////////////// 
        private double contourThresh = 100; //stores alpha for thread access
        private List<Hsv> lowerRange = new List<Hsv>();
        private List<Hsv> upperRange = new List<Hsv>();
        private List<Bgr> markerColor = new List<Bgr>();
        //////////////////////////////////////////////////////////////////////

        // COMMON OBJECTS /////////////////////////////////
        private IFrameInfo frameInfo = new FrameInfoImpl();
        private Dictionary<int, Queue<PointF>> pastVelocities = new Dictionary<int, Queue<PointF>>();

        private ImageViewer viewer;
        private Capture capture;

        private bool runThread;

        private MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 1.0, 1.0);
        //////////////////////////////////////////////////////////////////////

        // DELEGATES AND EVENTS /////////////////////////////////
        public delegate void CalibratedDelegate(IFrameInfo info);
        public delegate void FrameUpdatedDelegate(IFrameInfo info);
        public delegate void ObjectDetectedDelegate(IFrameObject info);
        public delegate void ObjectLostDelegate(IFrameObject info);
        public delegate void GameStartedDelegate();
        public delegate void GameOverDelegate();
        
        public event CalibratedDelegate Calibrated;
        public event ObjectDetectedDelegate ObjectDetected;
        public event ObjectLostDelegate ObjectLost;
        public event FrameUpdatedDelegate FrameUpdated;
        public event GameStartedDelegate GameStarted;
        public event GameOverDelegate GameOver;
        //////////////////////////////////////////////////////////////////////

        // IFace Implementations /////////////////////////////////
        private class FrameInfoImpl : IFrameInfo
        {
            long IFrameInfo.frameNumber { get; set; }

            List<IFrameObject> IFrameInfo.frameObjects { get; set; }
        }

        private class FrameObjectImpl : IFrameObject
        {
            int IFrameObject.ID { get; set; } 

            PointF IFrameObject.pos { get; set; }

            PointF IFrameObject.vel { get; set; }

            int IFrameObject.size { get; set; }

            bool IFrameObject.aiControlled { get; set; }

            long IFrameObject.lastFrame { get; set; }

            long IFrameObject.firstFrame { get; set; }

            public float distanceTo(IFrameObject other)
            {
                return (float)(Math.Sqrt((Math.Pow(Math.Abs(((IFrameObject)this).pos.X - other.pos.X), 2) + Math.Pow(Math.Abs(((IFrameObject)this).pos.Y - other.pos.Y), 2))));
            }
        }
        //////////////////////////////////////////////////////////////////////

        public PositionDetection()
        {
            viewer = new ImageViewer();     //create an image viewer
            capture = new Capture();        //create a camera captue  
            runThread = true;

            selectedId = -1;
            subtractor = new BackgroundSubstractorMOG( 3, 4, 0.8, 0.5 );

            frameInfo.frameObjects = new List<IFrameObject>();
            frameInfo.frameNumber = 0;

            window = new calibrationLabel();

            PictureBox pictureBox = window.getPictureBox();
            pictureBox.MouseDown += pictureBox_MouseDown;
            pictureBox.MouseUp += pictureBox_MouseUp;
            pictureBox.MouseMove += pictureBox_MouseMove;

            window.getStartButton().Click += StartButton_Click;

            Thread windowThread = new Thread(new ThreadStart(windowThreadFunc));
            windowThread.Start();
        }

        void StartButton_Click(object sender, EventArgs e)
        {
            window.getStartButton().Enabled = false;
            window.setGameWindowLabel("");
            runGame = true;

            if ( null != GameStarted )
            {
                GameStarted();
            }
        }

        public void shotHit(int score)
        {
            window.changeScoreLabel(score.ToString());
            messageString = "Player hit Mini-Car!";
        }

        void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (null != this.calibObj)
            {
                Point vec = new Point(e.X - (int) this.calibObj.pos.X, e.Y - (int) this.calibObj.pos.Y);
                double len = Math.Max(Math.Sqrt(vec.X * vec.X + vec.Y + vec.Y), MIN_OBJ_SIZE);

                if (Double.IsInfinity(len) || Double.IsNaN(len))
                {
                    len = MIN_OBJ_SIZE;
                }
                else if (len > MAX_CAR_SIZE)
                {
                    len = MAX_CAR_SIZE;
                }

                this.calibObj.size = (int)len;
            }
        }

        void switchCalibratingCar(int carId)
        {
            selectedId = carId;

            Bgr bgrLow = this.bgrLowPlayerCar;
            Bgr bgrHigh = this.bgrHighPlayerCar;

            if (1 == selectedId)
            {
                bgrLow = this.bgrLowAiCar;
                bgrHigh = this.bgrHighAiCar;

                window.setCalibratingLabel("Ai-Car calibration");
                window.showAiCarCalibrationParams();
            }
            else
            {
                window.setEdgeCount(edgeCount);
                window.setMinArea(minArea);
                window.setAngleTolerance(angleTolerance);
                window.setEdgeAccuracy(accuarcy);
                window.setCannyThresh(cannyThresh);
                window.setCannyLinking(cannyLinking);

                window.setCalibratingLabel("Player-Car calibration");
                window.showPlayerCarCalibrationParams();
            }

            double range = (bgrHigh.Red - bgrLow.Red) / 2;
            Bgr bgr = new Bgr(bgrLow.Red + range, bgrLow.Green + range, bgrLow.Blue + range);

            window.setRValue((int)bgr.Red);
            window.setGValue((int)bgr.Green);
            window.setBValue((int)bgr.Blue);
            window.setRange((int)range);
        }

        void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if ( false == calibratedFlag )
            {
                switchCalibratingCar(this.calibObj.ID);

                this.calibObj = null;

                if (MIN_OBJ_COUNT == frameInfo.frameObjects.Count)
                {
                    calibratedFlag = true;

                    window.setGameWindowLabel("When finished calibration press Start-Button.");
                    window.enableStartButton();

                    if ( null != Calibrated)
                    {
                        Calibrated(frameInfo);
                    }
                }
            }
        }

        void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            // only MIN_OBJ_COUNT objects for now
            if (MIN_OBJ_COUNT == frameInfo.frameObjects.Count)
            {
                IFrameObject selectedCar = null;

                foreach (IFrameObject o in frameInfo.frameObjects)
                {
                    PointF vec = new PointF(o.pos.X - e.Location.X, o.pos.Y - e.Location.Y);
                    double distance = Math.Sqrt(vec.X * vec.X + vec.Y * vec.Y);

                    if (distance < o.size)
                    {
                        selectedCar = o;
                    }
                }

                if (null != selectedCar)
                {
                    switchCalibratingCar(selectedCar.ID);
                }

                return;
            }

            IFrameObject obj = new FrameObjectImpl();
            obj.ID = frameInfo.frameObjects.Count;                        // id 0 is always player controlled object
            obj.pos = new PointF(e.Location.X, e.Location.Y);
            obj.vel = new PointF(0.0f, 0.0f);
            obj.aiControlled = (0 != frameInfo.frameObjects.Count);          // id 0 is always player controlled object
            obj.lastFrame = 0;
            obj.firstFrame = 0;
            obj.size = MIN_OBJ_SIZE;

            if (obj.aiControlled)
            {
                aiCar = obj;
            }
            else
            {
                playerCar = obj;
                window.setGameWindowLabel("please mark Ai-Car...");
            }

            frameInfo.frameObjects.Add( obj );

            pastVelocities.Add(obj.ID, new Queue<PointF>());

            this.calibObj = obj;
        }

        void windowThreadFunc()
        {
            window.Show();

            while (runThread)
            {
                int loopTime = Environment.TickCount;

                // need to pump events of this thread
                Application.DoEvents();
                
                if (null != messageString)
                {
                    if (0 == messageTime)
                    {
                        window.showMessage(messageString);
                        messageTime = Environment.TickCount + SHOW_MESSAGE_TIME;
                    }
                    else if (Environment.TickCount >= messageTime)
                    {
                        messageTime = 0;
                        window.showMessage("");
                        messageString = null;
                    }
                }

                Image<Bgr, Byte> mainImg = capture.QueryFrame();

                Image<Gray, Byte> calibrationImg = null;

                Image<Gray, Byte> contourDetectImg = null;
                Image<Gray, Byte> aiColorDetectImg = null;
                Image<Gray, Byte> playerColorDetectImg = null;

                int delta = window.getRange();

                int r = window.getRValue();
                int g = window.getGValue();
                int b = window.getBValue();

                Bgr low = new Bgr(r - delta, g - delta, b - delta);
                Bgr high = new Bgr(r + delta, g + delta, b + delta);

                // player-Car is marked, calculate position-updates
                if ( null != playerCar && null == this.calibObj )
                {
                    edgeCount = window.getEdgeCount();
                    minArea = window.getMinArea();
                    angleTolerance = window.getAngleTolerance();
                    accuarcy = window.getEdgeAccuracy();
                    cannyThresh = window.getCannyThresh();
                    cannyLinking = window.getCannyLinking();

                    if (0 == selectedId)
                    {
                        bgrLowPlayerCar = low;
                        bgrHighPlayerCar = high;
                    }

                    // 1st: detect by blob - for high speed
                    detectCarByBlob(mainImg, playerCar);
                    // 2nd: detect by contour - for low and medium speed
                    contourDetectImg = detectCarByRectContour(mainImg, playerCar, 
                        edgeCount, minArea, angleTolerance, accuarcy,
                        cannyThresh, cannyLinking);
                    // 3rd: detect by color - for ultimate fallback
                    playerColorDetectImg = detectCarByRGBColor(mainImg, playerCar, bgrLowPlayerCar, bgrHighPlayerCar);
                }
               
                
                // ai-Car is marked, calculate position-updates
                if (null != aiCar && null == this.calibObj )
                {
                    if (1 == selectedId)
                    {
                        bgrLowAiCar = low;
                        bgrHighAiCar = high;
                    }

                    aiColorDetectImg = detectCarByRGBColor(mainImg, aiCar, bgrLowAiCar, bgrHighAiCar);
                }

                if ( 0 == selectedId )
                {
                    if (window.isColorCalibrationSelected())
                    {
                        calibrationImg = playerColorDetectImg;
                    }
                    else
                    {
                        calibrationImg = contourDetectImg;
                    }
                }
                else
                {
                    calibrationImg = aiColorDetectImg;
                }

                if (null != calibrationImg)
                {
                    window.updateDetectorImage(calibrationImg.ToBitmap());
                }

                foreach (IFrameObject obj in frameInfo.frameObjects)
                {
                    Color color = Color.Blue;

                    if (false == obj.aiControlled)
                    {
                        color = Color.Red;
                    }

                    mainImg.Draw(new CircleF(obj.pos, obj.size), new Bgr(color), 2);
                    mainImg.Draw(new LineSegment2DF(obj.pos, new PointF(obj.pos.X + obj.vel.X * 5.0f, obj.pos.Y + obj.vel.Y * 5.0f)), new Bgr(color), 5);
                }

                window.updateMainImage(mainImg.ToBitmap());

                if (runGame)
                {
                    frameInfo.frameNumber++;

                    if (FrameUpdated != null)
                    {
                        FrameUpdated(frameInfo);
                    }

                    timeLeft -= (Environment.TickCount - loopTime);
                    if ( timeLeft <= 0.0 )
                    {
                        timeLeft = PLAYING_TIME;
                        runGame = false;
                        window.enableStartButton();
                        window.setGameWindowLabel("Please press Start-Button to restart Game.");

                        if (null != GameOver)
                        {
                            GameOver();
                        }
                    }

                    window.setTimeLeftLabel(((double)timeLeft / 1000.0).ToString());
                }
            }

            runThread = false;
        }

        Image<Gray, Byte> detectCarByBlob(Image<Bgr, Byte> img, IFrameObject car)
        {
            // learning rate defines how fast the movement 'decays'
            subtractor.Update(img, 0.06);

            Image<Gray, Byte> fg = subtractor.ForgroundMask;

            // REMOVE NOISE
            IntPtr element = CvInvoke.cvCreateStructuringElementEx(2, 2, 0, 0, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE, IntPtr.Zero);
            CvInvoke.cvErode(fg, fg, element, 3);
            CvInvoke.cvDilate(fg, fg, element, 3);

            CvInvoke.cvMorphologyEx(fg.Ptr, fg.Ptr, IntPtr.Zero, element, CV_MORPH_OP.CV_MOP_OPEN, 1);

            CvBlobs blobs = new CvBlobs();
            CvBlobDetector bDetector = new CvBlobDetector();
            bDetector.Detect(fg, blobs);

            // match blobs in area of player-car to car and advance position
            matchBlobsToObj(car, blobs);

            return fg;
        }

        Image<Gray, Byte> detectCarByRectContour(Image<Bgr, Byte> img, IFrameObject car, int edgeCount, int minArea, int angleTolerance, int accuracy, int cannyThresh, int cannyLinking )
        {
            storage.Clear();

            double accuracyFactor = MAX_ACCURACY * (accuracy / 100.0);

            int minAngle = (int)(360.0 / (double)edgeCount) - angleTolerance;
            int maxAngle = (int)(360.0 / (double)edgeCount) + angleTolerance;

            Image<Gray, Byte> cannyEdges = img.Convert<Gray, Byte>().Canny(new Gray(cannyThresh), new Gray(cannyLinking));

            for (Contour<Point> contours = cannyEdges.FindContours(); contours != null; contours = contours.HNext)
            {
                Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * accuracyFactor, storage);

                if (currentContour.Area < minArea )
                {
                    continue;
                }

                bool isValid = true;

                if (currentContour.Total == edgeCount)
                {
                    Point[] pts = currentContour.ToArray();
                    LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                    for (int i = 0; i < edges.Length; i++)
                    {
                        double angle = Math.Abs(
                            edges[(i + 1) % edges.Length].GetExteriorAngleDegree(edges[i]));
                        if (angle < minAngle || angle > maxAngle)
                        {
                            isValid = false;
                            break;
                        }
                    }

                    if (isValid)
                    {
                        Point[] points = currentContour.ToArray();
                        cannyEdges.DrawPolyline(points, true, new Gray(255), 5);
                        Point p = new Point((int)currentContour.GetMinAreaRect().center.X,
                            (int)currentContour.GetMinAreaRect().center.Y);

                        cannyEdges.Draw("Player", ref this.font, p, new Gray(255));

                        PointF pos = new PointF(currentContour.GetMinAreaRect().center.X,
                            currentContour.GetMinAreaRect().center.Y);

                        updatePositionAndVelocity(car, pos);
                    }
                }
            }

            return cannyEdges;
        }

        Image<Gray, Byte> detectCarByRGBColor(Image<Bgr, Byte> img, IFrameObject car, Bgr low, Bgr high)
        { 
            Image<Gray, Byte> detectorImg = img.InRange(low, high);

            IntPtr element = CvInvoke.cvCreateStructuringElementEx(2, 2, 0, 0, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE, IntPtr.Zero);
            CvInvoke.cvErode(detectorImg, detectorImg, element, 3);
            CvInvoke.cvDilate(detectorImg, detectorImg, element, 3);

            CvInvoke.cvMorphologyEx(detectorImg.Ptr, detectorImg.Ptr, IntPtr.Zero, element, CV_MORPH_OP.CV_MOP_OPEN, 1);

            CvBlobs blobs = new CvBlobs();
            CvBlobDetector bDetector = new CvBlobDetector();
            bDetector.Detect(detectorImg, blobs);

            CvBlob maxBlob = null;

            foreach (CvBlob blob in blobs.Values)
            {
                if (blob.Area < 10)
                {
                    continue;
                }

                if (null == maxBlob)
                {
                    maxBlob = blob;
                }
                else
                {
                    if ( blob.Area > maxBlob.Area )
                    {
                        maxBlob = blob;
                    }
                }
            }

            PointF? pos = null;

            if (null != maxBlob )
            {
                pos = new PointF(maxBlob.Centroid.X, maxBlob.Centroid.Y);
            }

            updatePositionAndVelocity(car, pos);

            return detectorImg;
        }

        void updatePositionAndVelocity(IFrameObject obj, PointF? newPos)
        {
            Queue<PointF> velocityHistory = pastVelocities[obj.ID];

            if (null != newPos)
            {
                PointF newVel = new PointF(newPos.Value.X - obj.pos.X, newPos.Value.Y - obj.pos.Y);
                obj.pos = newPos.Value;

                if (velocityHistory.Count == MAX_MEDIAN_VELOCITIES)
                {
                    velocityHistory.Dequeue();
                }

                velocityHistory.Enqueue(newVel);
            }
            else
            {
                if (velocityHistory.Count > 0)
                {
                    velocityHistory.Dequeue();
                }

                // velocityHistory.Clear();
            }

            PointF averageVel = new PointF();
            foreach (PointF p in velocityHistory)
            {
                averageVel.X += p.X;
                averageVel.Y += p.Y;
            }

            if (0 < velocityHistory.Count)
            {
                averageVel.X /= velocityHistory.Count;
                averageVel.Y /= velocityHistory.Count;
            }

            obj.vel = averageVel;

        }


        // PROTOTYPING CODE, LEFT AS AN EXAMPLE ///////////////////////////////////////////////////////////////////
        Image<Gray, Byte> detectByBlob(Image<Bgr, Byte> img)
        {
            // learning rate defines how fast the movement 'decays'
            subtractor.Update(img, 0.06);

            Image<Gray, Byte> fg = subtractor.ForgroundMask;

            // REMOVE NOISE
            IntPtr element = CvInvoke.cvCreateStructuringElementEx(2, 2, 0, 0, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE, IntPtr.Zero);
            CvInvoke.cvErode(fg, fg, element, 3);
            CvInvoke.cvDilate(fg, fg, element, 3);

            CvInvoke.cvMorphologyEx( fg.Ptr, fg.Ptr, IntPtr.Zero, element, CV_MORPH_OP.CV_MOP_OPEN, 1 );

            CvBlobs blobs = new CvBlobs();
            CvBlobDetector bDetector = new CvBlobDetector();
            bDetector.Detect(fg, blobs);

            foreach (IFrameObject obj in frameInfo.frameObjects)
            {
                matchBlobsToObj( obj, blobs );
            }

            return fg;
        }

        // PROTOTYPING CODE, LEFT AS AN EXAMPLE
        void matchBlobsToObj( IFrameObject obj, CvBlobs blobs )
        {
            float count = 0;
            PointF pointsSum = new PointF();

            PointF center = new PointF(obj.pos.X, obj.pos.Y);

            double radius = obj.size;
            double radiusSquared = radius * radius;

            foreach (CvBlob blob in blobs.Values)
            {
                if (blob.Area < 10)
                {
                    continue;
                }

                // normalize: move into center
                PointF n = new PointF(blob.Centroid.X - center.X, blob.Centroid.Y - center.Y);

                double v = n.X * n.X + n.Y * n.Y;
                // test whether point is inside or on border of elipsoid
                if (v <= radiusSquared)
                {
                    pointsSum.X += blob.Centroid.X;
                    pointsSum.Y += blob.Centroid.Y;

                    count++;
                }
            }

            PointF? p = null;

            // blobs around object dected => update position and velocity
            if (count > 0)
            {
                p = new PointF(pointsSum.X / count, pointsSum.Y / count);
            }

            updatePositionAndVelocity(obj, p);
        }

        // PROTOTYPING CODE, LEFT AS AN EXAMPLE
        void detectCarsByShapes(Image<Gray, Byte> img, ImageViewer viewer)
        {
            frameInfo.frameNumber++;
            
            this.detectContours(img);

            this.detectShapes(img);

            this.processAndSendFrameInfo(img);
        }

        // PROTOTYPING CODE, LEFT AS AN EXAMPLE
        Image<Gray, Byte> detectContours(Image<Gray, Byte> img)
        {
            detectedContoursInFrame.Clear();
            triangleConturs.Clear();

            storage.Clear();

            //Convert the image to grayscale and filter out the noise
            //Image<Gray, Byte> gray = img.Convert<Gray, Byte>().PyrDown().PyrUp();
            Image<Gray, Byte> cannyEdges = img.Canny(cannyThreshold, cannyThresholdLinking);
   
            for (Contour<Point> contours = cannyEdges.FindContours(); contours != null; contours = contours.HNext)
            {
                Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.1, storage);

                if ( currentContour.Area < 50 )
                {
                    continue;
                }

                bool isValid = true;

                //Point[] points = currentContour.ToArray();
                //cannyEdges.DrawPolyline(points, true, new Gray(255), 2);

                if (currentContour.Total == 3) //The contour has 3 vertices, it is a triangle
                {
                    triangleConturs.Add(currentContour);
                    isValid = false;
                }
                else if (currentContour.Total > 3 && currentContour.Total <= 6)
                {
                    Tuple<int, int> angleTuple = angleRanges[currentContour.Total];

                    Point[] pts = currentContour.ToArray();
                    LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                    for (int i = 0; i < edges.Length; i++)
                    {
                        double angle = Math.Abs(
                            edges[(i + 1) % edges.Length].GetExteriorAngleDegree(edges[i]));
                        if (angle < angleTuple.Item1 || angle > angleTuple.Item2)
                        {
                            isValid = false;
                            break;
                        }
                    }

                    if (isValid)
                    {
                        Point[] points = currentContour.ToArray();
                        cannyEdges.DrawPolyline(points, true, new Gray(255), 2);
                        
                        MCvFont f = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 1.0, 1.0);
                        Point p = new Point((int)currentContour.GetMinAreaRect().center.X, 
                            (int)currentContour.GetMinAreaRect().center.Y );

                        //img.Draw(cont.BoundingRectangle, new Gray(255), 2);
                        cannyEdges.Draw("Car " + currentContour.Total, ref f, p, new Gray(255));

                        if (false == detectedContoursInFrame.ContainsKey(currentContour.Total))
                        {
                            detectedContoursInFrame.Add(currentContour.Total, currentContour);
                        }
                        else
                        {
                            Contour<Point> genericContour = detectedContoursInFrame[currentContour.Total];
                            if (currentContour.Area > genericContour.Area)
                            {
                                detectedContoursInFrame[currentContour.Total] = genericContour;
                            }
                        }
                    }
                }
            }

            return cannyEdges;
        }

        // PROTOTYPING CODE, LEFT AS AN EXAMPLE
        void detectShapes(Image<Gray, Byte> img)
        {
            foreach (KeyValuePair<int, Contour<Point>> pair in detectedContoursInFrame)
            {
                int trisInsideCount = 0;
                Contour<Point> cont = pair.Value;

                foreach (Contour<Point> tri in triangleConturs)
                {
                    Point centerPoint = new Point((int)tri.GetMinAreaRect().center.X, (int)tri.GetMinAreaRect().center.Y);
                    if (cont.BoundingRectangle.Contains(centerPoint))
                    {
                        trisInsideCount++;
                    }

                    bool allPointsInside = true;
                    Point[] points = tri.ToArray();

                    foreach (Point p in points)
                    {
                        if (false == cont.BoundingRectangle.Contains(p))
                        {
                            allPointsInside = false;
                            break;
                        }
                    }

                    if (allPointsInside)
                    {
                        trisInsideCount++;
                        //break;
                    }
                }

                if (1 <= trisInsideCount)
                {
                    img.Draw("Car ", ref this.font, new Point((int)cont.GetMinAreaRect().center.X, (int)cont.GetMinAreaRect().center.Y), new Gray(255));

                    if (false == stableKnownObjects.ContainsKey(pair.Key))
                    {
                        IFrameObject newFrameObject = new FrameObjectImpl();
                        newFrameObject.ID = pair.Key;
                        newFrameObject.pos = new PointF(cont.GetMinAreaRect().center.X / img.Size.Width, cont.GetMinAreaRect().center.Y / img.Size.Height);
                        newFrameObject.vel = new PointF(0.0f, 0.0f);
                        newFrameObject.aiControlled = true;
                        newFrameObject.lastFrame = frameInfo.frameNumber;
                        newFrameObject.firstFrame = frameInfo.frameNumber;

                        stableKnownObjects.Add(pair.Key, newFrameObject);

                        //Console.WriteLine("Detected new Object " + newFrameObject.ID + ", awaiting promotion... " );
                    }
                    else
                    {
                        PointF newNormalizedPos = new PointF(cont.GetMinAreaRect().center.X / img.Size.Width, cont.GetMinAreaRect().center.Y / img.Size.Height);

                        IFrameObject fo = stableKnownObjects[pair.Key];
                        // update velocity based upon new position
                        fo.vel = new PointF((newNormalizedPos.X - fo.pos.X) / (frameInfo.frameNumber - fo.lastFrame),
                            (newNormalizedPos.Y - fo.pos.Y) / (frameInfo.frameNumber - fo.lastFrame));
                        fo.pos = newNormalizedPos;

                        // this object is still awaiting promotion to stable dected
                        if (0 != fo.firstFrame)
                        {
                            //Console.WriteLine("Detected Object awaiting promotion: " + fo.ID);

                            // object awaiting promotion to stable needs to be visible for OBJECT_DETECTED_DURATION frames
                            // if not visible for one frame, reset firstFrame counter
                            if (frameInfo.frameNumber - fo.lastFrame > 1)
                            {
                                fo.firstFrame = frameInfo.frameNumber;
                            }
                            // visible for OBJECT_DETECTED_DURATION frames in a row ?
                            else if (frameInfo.frameNumber - fo.firstFrame >= OBJECT_DETECTED_DURATION)
                            {
                                // promote to stable detected
                                fo.firstFrame = 0;

                                // Console.WriteLine("Promoted Object " + fo.ID);

                                if (null != ObjectDetected)
                                {
                                    ObjectDetected(fo);
                                }
                            }
                        }
                        else
                        {
                            //Console.WriteLine("Detected promoted Object " + fo.ID);
                        }

                        fo.lastFrame = frameInfo.frameNumber;

                    }
                }
            }
        }

        // PROTOTYPING CODE, LEFT AS AN EXAMPLE
        void processAndSendFrameInfo(Image<Gray, Byte> img)
        {
            frameInfo.frameObjects.Clear();

            Dictionary<int, IFrameObject>.Enumerator iter = stableKnownObjects.GetEnumerator();
            while (iter.MoveNext())
            {
                IFrameObject fo = iter.Current.Value;

                // already marked
                if (frameInfo.frameNumber - fo.lastFrame >= OBJECT_LOST_DURATION)
                {
                    // object definitely lost
                    stableKnownObjects.Remove(fo.ID);
                    iter = stableKnownObjects.GetEnumerator();

                    // dont send event when object was not yet promoted to stable detected (otherwise double lost-event)
                    if (0 != fo.firstFrame)
                    {
                        continue;
                    }

                    if (null != ObjectLost)
                    {
                        ObjectLost(fo);
                    }
                }
                else
                {
                    frameInfo.frameObjects.Add(fo);

                    MCvFont f = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 1.0, 1.0);
                    Point p = new Point((int)(fo.pos.X * (float)img.Size.Width), (int)(fo.pos.Y * (float)img.Size.Height));

                    //img.Draw(cont.BoundingRectangle, new Gray(255), 2);
                    img.Draw("Car " + fo.ID, ref f, p, new Gray(255));
                }
            }

            if (FrameUpdated != null)
            {
                FrameUpdated(frameInfo);
            }
        }

        // PROTOTYPING CODE, LEFT AS AN EXAMPLE
        void detectCarsByColor(Image<Bgr, Byte> camImg, ImageViewer viewer)
        {
            //camImg._SmoothGaussian(11);

            FrameInfoImpl frameInfo = new FrameInfoImpl();
            //gray.Convert<Gray, Byte>()._SmoothGaussian(4);
            for (int i = 0; i < lowerRange.Count; ++i)
            {
                using (Image<Hsv, byte> hsv = camImg.Convert<Hsv, byte>())
                {
                    Hsv low = lowerRange.ElementAt<Hsv>(i);
                    Hsv high = upperRange.ElementAt<Hsv>(i);

                    Image<Gray, Byte> rangeResult = hsv.InRange(low, high);
                    rangeResult = rangeResult.Erode(5);
                    rangeResult = rangeResult.Dilate(5);

                    storage.Clear();

                    Contour<Point> maxContour = null;

                    for (Contour<Point> contours = rangeResult.FindContours(
                            Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE,
                            Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST,
                            storage);
                        contours != null;
                        contours = contours.HNext)
                    {
                        //Create a contour for the current variable for us to work with
                        Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);

                        if (null == maxContour && currentContour.Area > contourThresh)
                        {
                            maxContour = currentContour;
                        }

                        //Draw the detected contour on the image
                        if (currentContour.Area > contourThresh && currentContour.Area > maxContour.Area) //only consider contours with area greater than 100 as default then take from form control
                        {
                            maxContour = currentContour;
                        }
                    }

                    if (null != maxContour)
                    {
                        Rectangle rect = maxContour.BoundingRectangle;
                        Point center = new Point(rect.Location.X + (rect.Width / 2), rect.Location.Y + (rect.Height / 2));

                        // NOTE: corner on the top left
                        PointF normalizedPos = new PointF();
                        normalizedPos.X = (float)center.X / (float)camImg.Size.Width;
                        normalizedPos.Y = (float)center.Y / (float)camImg.Size.Height;

                        MCvFont f = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 1.0, 1.0);
                        camImg.Draw(maxContour.BoundingRectangle, markerColor.ElementAt<Bgr>(i), 2);
                        camImg.Draw("car " + normalizedPos, ref f, center, markerColor.ElementAt<Bgr>(i));

                        IFrameObject frameObject = new FrameObjectImpl();
                        frameObject.pos = new PointF(0.0f, 0.0f);
                        frameObject.vel = new PointF(0.0f, 0.0f);
                        frameObject.aiControlled = true;
                    }
                }
            }

            if (FrameUpdated != null)
            {
                FrameUpdated(frameInfo);
            }

            viewer.Image = camImg;
        }
    }
}
