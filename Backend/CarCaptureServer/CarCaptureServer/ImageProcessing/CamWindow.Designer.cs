﻿namespace CarCaptureServer.ImageProcessing
{
    partial class calibrationLabel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.scoreLabel = new System.Windows.Forms.Label();
            this.messageLabel = new System.Windows.Forms.Label();
            this.calibrationWindowLabel = new System.Windows.Forms.Label();
            this.gameWindowLabel = new System.Windows.Forms.Label();
            this.carCalibrationParams = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.rangeLabel = new System.Windows.Forms.Label();
            this.rangeChooser = new System.Windows.Forms.TrackBar();
            this.bLabel = new System.Windows.Forms.Label();
            this.gLabel = new System.Windows.Forms.Label();
            this.rLabel = new System.Windows.Forms.Label();
            this.bChooser = new System.Windows.Forms.TrackBar();
            this.gChooser = new System.Windows.Forms.TrackBar();
            this.rChooser = new System.Windows.Forms.TrackBar();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cannyLinkingLabel = new System.Windows.Forms.Label();
            this.cannyLinkingChooser = new System.Windows.Forms.TrackBar();
            this.cannyThreshLabel = new System.Windows.Forms.Label();
            this.cannyThreshChooser = new System.Windows.Forms.TrackBar();
            this.label5 = new System.Windows.Forms.Label();
            this.minAreaSizeLabel = new System.Windows.Forms.Label();
            this.edgeAccuracyLabel = new System.Windows.Forms.Label();
            this.minAreaSizeChooser = new System.Windows.Forms.TrackBar();
            this.edgeAccuaryChooser = new System.Windows.Forms.TrackBar();
            this.angleToleranceLabel = new System.Windows.Forms.Label();
            this.angleToleranceChooser = new System.Windows.Forms.TrackBar();
            this.rectangleRadioButton = new System.Windows.Forms.RadioButton();
            this.triangleRadioButton = new System.Windows.Forms.RadioButton();
            this.timeLeftLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.carCalibrationParams.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rangeChooser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bChooser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gChooser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rChooser)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cannyLinkingChooser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cannyThreshChooser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minAreaSizeChooser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edgeAccuaryChooser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.angleToleranceChooser)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 45);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(640, 480);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(671, 43);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(640, 480);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(152, 553);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Player-Score:";
            // 
            // scoreLabel
            // 
            this.scoreLabel.AutoSize = true;
            this.scoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoreLabel.Location = new System.Drawing.Point(352, 553);
            this.scoreLabel.Name = "scoreLabel";
            this.scoreLabel.Size = new System.Drawing.Size(29, 31);
            this.scoreLabel.TabIndex = 3;
            this.scoreLabel.Text = "0";
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.Location = new System.Drawing.Point(226, 24);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(0, 31);
            this.messageLabel.TabIndex = 4;
            // 
            // calibrationWindowLabel
            // 
            this.calibrationWindowLabel.AutoSize = true;
            this.calibrationWindowLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calibrationWindowLabel.Location = new System.Drawing.Point(777, 9);
            this.calibrationWindowLabel.Name = "calibrationWindowLabel";
            this.calibrationWindowLabel.Size = new System.Drawing.Size(294, 31);
            this.calibrationWindowLabel.TabIndex = 14;
            this.calibrationWindowLabel.Text = "awaiting Car-Marking...";
            // 
            // gameWindowLabel
            // 
            this.gameWindowLabel.AutoSize = true;
            this.gameWindowLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameWindowLabel.Location = new System.Drawing.Point(12, 7);
            this.gameWindowLabel.Name = "gameWindowLabel";
            this.gameWindowLabel.Size = new System.Drawing.Size(323, 33);
            this.gameWindowLabel.TabIndex = 15;
            this.gameWindowLabel.Text = "please mark Player-Car";
            this.gameWindowLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // carCalibrationParams
            // 
            this.carCalibrationParams.Controls.Add(this.tabPage1);
            this.carCalibrationParams.Controls.Add(this.tabPage2);
            this.carCalibrationParams.Location = new System.Drawing.Point(667, 534);
            this.carCalibrationParams.Name = "carCalibrationParams";
            this.carCalibrationParams.SelectedIndex = 0;
            this.carCalibrationParams.Size = new System.Drawing.Size(640, 326);
            this.carCalibrationParams.TabIndex = 16;
            this.carCalibrationParams.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.rangeLabel);
            this.tabPage1.Controls.Add(this.rangeChooser);
            this.tabPage1.Controls.Add(this.bLabel);
            this.tabPage1.Controls.Add(this.gLabel);
            this.tabPage1.Controls.Add(this.rLabel);
            this.tabPage1.Controls.Add(this.bChooser);
            this.tabPage1.Controls.Add(this.gChooser);
            this.tabPage1.Controls.Add(this.rChooser);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(632, 300);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Color Detection";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // rangeLabel
            // 
            this.rangeLabel.AutoSize = true;
            this.rangeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rangeLabel.Location = new System.Drawing.Point(3, 159);
            this.rangeLabel.Name = "rangeLabel";
            this.rangeLabel.Size = new System.Drawing.Size(93, 24);
            this.rangeLabel.TabIndex = 20;
            this.rangeLabel.Text = "Range (5)";
            // 
            // rangeChooser
            // 
            this.rangeChooser.Location = new System.Drawing.Point(102, 159);
            this.rangeChooser.Maximum = 50;
            this.rangeChooser.Minimum = 5;
            this.rangeChooser.Name = "rangeChooser";
            this.rangeChooser.Size = new System.Drawing.Size(512, 45);
            this.rangeChooser.TabIndex = 19;
            this.rangeChooser.Value = 5;
            this.rangeChooser.Scroll += new System.EventHandler(this.rangeChooser_Scroll_1);
            // 
            // bLabel
            // 
            this.bLabel.AutoSize = true;
            this.bLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bLabel.Location = new System.Drawing.Point(30, 119);
            this.bLabel.Name = "bLabel";
            this.bLabel.Size = new System.Drawing.Size(49, 24);
            this.bLabel.TabIndex = 18;
            this.bLabel.Text = "B (0)";
            // 
            // gLabel
            // 
            this.gLabel.AutoSize = true;
            this.gLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gLabel.Location = new System.Drawing.Point(28, 67);
            this.gLabel.Name = "gLabel";
            this.gLabel.Size = new System.Drawing.Size(51, 24);
            this.gLabel.TabIndex = 17;
            this.gLabel.Text = "G (0)";
            // 
            // rLabel
            // 
            this.rLabel.AutoSize = true;
            this.rLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rLabel.Location = new System.Drawing.Point(28, 17);
            this.rLabel.Name = "rLabel";
            this.rLabel.Size = new System.Drawing.Size(50, 24);
            this.rLabel.TabIndex = 16;
            this.rLabel.Text = "R (0)";
            // 
            // bChooser
            // 
            this.bChooser.Location = new System.Drawing.Point(92, 108);
            this.bChooser.Maximum = 255;
            this.bChooser.Name = "bChooser";
            this.bChooser.Size = new System.Drawing.Size(534, 45);
            this.bChooser.TabIndex = 15;
            this.bChooser.Scroll += new System.EventHandler(this.bChooser_Scroll_1);
            // 
            // gChooser
            // 
            this.gChooser.Location = new System.Drawing.Point(92, 57);
            this.gChooser.Maximum = 255;
            this.gChooser.Name = "gChooser";
            this.gChooser.Size = new System.Drawing.Size(534, 45);
            this.gChooser.TabIndex = 14;
            this.gChooser.Scroll += new System.EventHandler(this.gChooser_Scroll_1);
            // 
            // rChooser
            // 
            this.rChooser.Location = new System.Drawing.Point(92, 6);
            this.rChooser.Maximum = 255;
            this.rChooser.Name = "rChooser";
            this.rChooser.Size = new System.Drawing.Size(534, 45);
            this.rChooser.TabIndex = 13;
            this.rChooser.Scroll += new System.EventHandler(this.rChooser_Scroll_1);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cannyLinkingLabel);
            this.tabPage2.Controls.Add(this.cannyLinkingChooser);
            this.tabPage2.Controls.Add(this.cannyThreshLabel);
            this.tabPage2.Controls.Add(this.cannyThreshChooser);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.minAreaSizeLabel);
            this.tabPage2.Controls.Add(this.edgeAccuracyLabel);
            this.tabPage2.Controls.Add(this.minAreaSizeChooser);
            this.tabPage2.Controls.Add(this.edgeAccuaryChooser);
            this.tabPage2.Controls.Add(this.angleToleranceLabel);
            this.tabPage2.Controls.Add(this.angleToleranceChooser);
            this.tabPage2.Controls.Add(this.rectangleRadioButton);
            this.tabPage2.Controls.Add(this.triangleRadioButton);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(632, 300);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Contour Detection";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cannyLinkingLabel
            // 
            this.cannyLinkingLabel.AutoSize = true;
            this.cannyLinkingLabel.Location = new System.Drawing.Point(12, 262);
            this.cannyLinkingLabel.Name = "cannyLinkingLabel";
            this.cannyLinkingLabel.Size = new System.Drawing.Size(95, 13);
            this.cannyLinkingLabel.TabIndex = 15;
            this.cannyLinkingLabel.Text = "Canny Linking (10)";
            // 
            // cannyLinkingChooser
            // 
            this.cannyLinkingChooser.Location = new System.Drawing.Point(133, 249);
            this.cannyLinkingChooser.Maximum = 255;
            this.cannyLinkingChooser.Minimum = 1;
            this.cannyLinkingChooser.Name = "cannyLinkingChooser";
            this.cannyLinkingChooser.Size = new System.Drawing.Size(464, 45);
            this.cannyLinkingChooser.TabIndex = 14;
            this.cannyLinkingChooser.Value = 50;
            this.cannyLinkingChooser.Scroll += new System.EventHandler(this.cannyLinkingChooser_Scroll);
            // 
            // cannyThreshLabel
            // 
            this.cannyThreshLabel.AutoSize = true;
            this.cannyThreshLabel.Location = new System.Drawing.Point(12, 212);
            this.cannyThreshLabel.Name = "cannyThreshLabel";
            this.cannyThreshLabel.Size = new System.Drawing.Size(94, 13);
            this.cannyThreshLabel.TabIndex = 11;
            this.cannyThreshLabel.Text = "Canny Thresh (10)";
            // 
            // cannyThreshChooser
            // 
            this.cannyThreshChooser.Location = new System.Drawing.Point(133, 198);
            this.cannyThreshChooser.Maximum = 255;
            this.cannyThreshChooser.Minimum = 1;
            this.cannyThreshChooser.Name = "cannyThreshChooser";
            this.cannyThreshChooser.Size = new System.Drawing.Size(464, 45);
            this.cannyThreshChooser.TabIndex = 10;
            this.cannyThreshChooser.Value = 50;
            this.cannyThreshChooser.Scroll += new System.EventHandler(this.cannyThreshChooser_Scroll);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Shape";
            // 
            // minAreaSizeLabel
            // 
            this.minAreaSizeLabel.AutoSize = true;
            this.minAreaSizeLabel.Location = new System.Drawing.Point(14, 111);
            this.minAreaSizeLabel.Name = "minAreaSizeLabel";
            this.minAreaSizeLabel.Size = new System.Drawing.Size(93, 13);
            this.minAreaSizeLabel.TabIndex = 7;
            this.minAreaSizeLabel.Text = "Min Area Size (10)";
            // 
            // edgeAccuracyLabel
            // 
            this.edgeAccuracyLabel.AutoSize = true;
            this.edgeAccuracyLabel.Location = new System.Drawing.Point(12, 157);
            this.edgeAccuracyLabel.Name = "edgeAccuracyLabel";
            this.edgeAccuracyLabel.Size = new System.Drawing.Size(95, 13);
            this.edgeAccuracyLabel.TabIndex = 6;
            this.edgeAccuracyLabel.Text = "Edge Accuracy (1)";
            // 
            // minAreaSizeChooser
            // 
            this.minAreaSizeChooser.Location = new System.Drawing.Point(133, 96);
            this.minAreaSizeChooser.Maximum = 200;
            this.minAreaSizeChooser.Minimum = 10;
            this.minAreaSizeChooser.Name = "minAreaSizeChooser";
            this.minAreaSizeChooser.Size = new System.Drawing.Size(464, 45);
            this.minAreaSizeChooser.TabIndex = 5;
            this.minAreaSizeChooser.Value = 50;
            this.minAreaSizeChooser.Scroll += new System.EventHandler(this.minAreaSizeChooser_Scroll);
            // 
            // edgeAccuaryChooser
            // 
            this.edgeAccuaryChooser.Location = new System.Drawing.Point(133, 147);
            this.edgeAccuaryChooser.Maximum = 100;
            this.edgeAccuaryChooser.Minimum = 1;
            this.edgeAccuaryChooser.Name = "edgeAccuaryChooser";
            this.edgeAccuaryChooser.Size = new System.Drawing.Size(464, 45);
            this.edgeAccuaryChooser.TabIndex = 4;
            this.edgeAccuaryChooser.Value = 1;
            this.edgeAccuaryChooser.Scroll += new System.EventHandler(this.edgeAccuaryChooser_Scroll);
            // 
            // angleToleranceLabel
            // 
            this.angleToleranceLabel.AutoSize = true;
            this.angleToleranceLabel.Location = new System.Drawing.Point(12, 58);
            this.angleToleranceLabel.Name = "angleToleranceLabel";
            this.angleToleranceLabel.Size = new System.Drawing.Size(100, 13);
            this.angleToleranceLabel.TabIndex = 3;
            this.angleToleranceLabel.Text = "Angle Tolerance (1)";
            // 
            // angleToleranceChooser
            // 
            this.angleToleranceChooser.Location = new System.Drawing.Point(133, 45);
            this.angleToleranceChooser.Maximum = 50;
            this.angleToleranceChooser.Minimum = 1;
            this.angleToleranceChooser.Name = "angleToleranceChooser";
            this.angleToleranceChooser.Size = new System.Drawing.Size(464, 45);
            this.angleToleranceChooser.TabIndex = 2;
            this.angleToleranceChooser.Value = 1;
            this.angleToleranceChooser.Scroll += new System.EventHandler(this.angleToleranceChooser_Scroll);
            // 
            // rectangleRadioButton
            // 
            this.rectangleRadioButton.AutoSize = true;
            this.rectangleRadioButton.Checked = true;
            this.rectangleRadioButton.Location = new System.Drawing.Point(210, 11);
            this.rectangleRadioButton.Name = "rectangleRadioButton";
            this.rectangleRadioButton.Size = new System.Drawing.Size(74, 17);
            this.rectangleRadioButton.TabIndex = 1;
            this.rectangleRadioButton.TabStop = true;
            this.rectangleRadioButton.Text = "Rectangle";
            this.rectangleRadioButton.UseVisualStyleBackColor = true;
            // 
            // triangleRadioButton
            // 
            this.triangleRadioButton.AutoSize = true;
            this.triangleRadioButton.Location = new System.Drawing.Point(133, 11);
            this.triangleRadioButton.Name = "triangleRadioButton";
            this.triangleRadioButton.Size = new System.Drawing.Size(63, 17);
            this.triangleRadioButton.TabIndex = 0;
            this.triangleRadioButton.Text = "Triangle";
            this.triangleRadioButton.UseVisualStyleBackColor = true;
            // 
            // timeLeftLabel
            // 
            this.timeLeftLabel.AutoSize = true;
            this.timeLeftLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLeftLabel.Location = new System.Drawing.Point(352, 606);
            this.timeLeftLabel.Name = "timeLeftLabel";
            this.timeLeftLabel.Size = new System.Drawing.Size(44, 31);
            this.timeLeftLabel.TabIndex = 18;
            this.timeLeftLabel.Text = "60";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(196, 606);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 31);
            this.label7.TabIndex = 17;
            this.label7.Text = "Time Left:";
            // 
            // startButton
            // 
            this.startButton.Enabled = false;
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButton.Location = new System.Drawing.Point(109, 688);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(390, 128);
            this.startButton.TabIndex = 19;
            this.startButton.Text = "Start!";
            this.startButton.UseVisualStyleBackColor = true;
            // 
            // calibrationLabel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1315, 869);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.timeLeftLabel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.carCalibrationParams);
            this.Controls.Add(this.gameWindowLabel);
            this.Controls.Add(this.calibrationWindowLabel);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.scoreLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "calibrationLabel";
            this.Text = "CamWindow";
            this.Load += new System.EventHandler(this.CamWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.carCalibrationParams.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rangeChooser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bChooser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gChooser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rChooser)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cannyLinkingChooser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cannyThreshChooser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minAreaSizeChooser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edgeAccuaryChooser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.angleToleranceChooser)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label scoreLabel;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Label calibrationWindowLabel;
        private System.Windows.Forms.Label gameWindowLabel;
        private System.Windows.Forms.TabControl carCalibrationParams;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label rangeLabel;
        private System.Windows.Forms.TrackBar rangeChooser;
        private System.Windows.Forms.Label bLabel;
        private System.Windows.Forms.Label gLabel;
        private System.Windows.Forms.Label rLabel;
        private System.Windows.Forms.TrackBar bChooser;
        private System.Windows.Forms.TrackBar gChooser;
        private System.Windows.Forms.TrackBar rChooser;
        private System.Windows.Forms.RadioButton rectangleRadioButton;
        private System.Windows.Forms.RadioButton triangleRadioButton;
        private System.Windows.Forms.Label angleToleranceLabel;
        private System.Windows.Forms.TrackBar angleToleranceChooser;
        private System.Windows.Forms.Label minAreaSizeLabel;
        private System.Windows.Forms.Label edgeAccuracyLabel;
        private System.Windows.Forms.TrackBar minAreaSizeChooser;
        private System.Windows.Forms.TrackBar edgeAccuaryChooser;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label timeLeftLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label cannyThreshLabel;
        private System.Windows.Forms.TrackBar cannyThreshChooser;
        private System.Windows.Forms.TrackBar cannyLinkingChooser;
        private System.Windows.Forms.Label cannyLinkingLabel;
        private System.Windows.Forms.Button startButton;
    }
}