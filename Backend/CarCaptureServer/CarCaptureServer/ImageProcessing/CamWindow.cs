﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace CarCaptureServer.ImageProcessing
{
    public partial class calibrationLabel : Form
    {
        private TabPage colorDetectionTabPage;
        private TabPage contourDetectionTabPage;
        
        public calibrationLabel()
        {
            InitializeComponent();  
        }

        public PictureBox getPictureBox()
        {
            return this.pictureBox1;
        }

        public Label getScoreLabel()
        {
            return this.scoreLabel;
        }

        public void updateMainImage(Image img)
        {
            pictureBox1.Image = img;
        }

        public void updateDetectorImage(Image img)
        {
            pictureBox2.Image = img;
        }

        private void CamWindow_Load(object sender, EventArgs e)
        {
            colorDetectionTabPage = this.carCalibrationParams.TabPages[0];
            contourDetectionTabPage = this.carCalibrationParams.TabPages[1];
        }

        internal bool isColorCalibrationSelected()
        {
            return this.carCalibrationParams.SelectedIndex == 0;
        }

        void carCalibrationParams_SelectedIndexChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        internal void changeScoreLabel(string p)
        {
            this.Invoke(new Action(() => { scoreLabel.Text = p; }));
        }

        internal void showMessage(string messageString)
        {
            this.messageLabel.Text = messageString;
        }

        internal int getRValue()
        {
            return this.rChooser.Value;
        }

        internal void setRValue(int r)
        {
            this.rChooser.Value = r;
            rChooser_Scroll_1(null, null);
        }

        internal int getGValue()
        {
            return this.gChooser.Value;
        }

        internal void setGValue(int g)
        {
            this.gChooser.Value = g;
            gChooser_Scroll_1(null, null);
        }

        internal int getBValue()
        {
            return this.bChooser.Value;
        }

        internal void setBValue(int b)
        {
            this.bChooser.Value = b;
            bChooser_Scroll_1(null, null);
        }


        internal int getRange()
        {
            return this.rangeChooser.Value;
        }

        internal void setRange(int i)
        {
            this.rangeChooser.Value = i;
            rangeChooser_Scroll_1(null, null);
        }

        internal void setCalibratingLabel(string p)
        {
            this.calibrationWindowLabel.Text = p;
        }

        internal void setGameWindowLabel(string p)
        {
            this.gameWindowLabel.Text = p;
        }

        private void rChooser_Scroll_1(object sender, EventArgs e)
        {
            this.rLabel.Text = "R (" + this.rChooser.Value + ")";
        }

        private void gChooser_Scroll_1(object sender, EventArgs e)
        {
            this.gLabel.Text = "G (" + this.gChooser.Value + ")";
        }

        private void bChooser_Scroll_1(object sender, EventArgs e)
        {
            this.bLabel.Text = "B (" + this.bChooser.Value + ")";
        }

        private void rangeChooser_Scroll_1(object sender, EventArgs e)
        {
            this.rangeLabel.Text = "Range (" + this.rangeChooser.Value + ")";
        }

        internal void showPlayerCarCalibrationParams()
        {
            this.carCalibrationParams.Visible = true;

            if (false == this.carCalibrationParams.TabPages.Contains(this.contourDetectionTabPage))
            {
                this.carCalibrationParams.TabPages.Insert(1, this.contourDetectionTabPage);
            }
        }

        internal void showAiCarCalibrationParams()
        {
            this.carCalibrationParams.Visible = true;

            if (this.carCalibrationParams.TabPages.Contains(this.contourDetectionTabPage))
            {
                this.carCalibrationParams.TabPages.Remove(this.contourDetectionTabPage);
            }
        }

        private void angleToleranceChooser_Scroll(object sender, EventArgs e)
        {
            angleToleranceLabel.Text = "Angle Tolerance (" + angleToleranceChooser.Value + ")";
        }

        private void edgeAccuaryChooser_Scroll(object sender, EventArgs e)
        {
            edgeAccuracyLabel.Text = "Edge Accuracy (" + edgeAccuaryChooser.Value + ")";
        }

        private void minAreaSizeChooser_Scroll(object sender, EventArgs e)
        {
            minAreaSizeLabel.Text = "Min Area (" + minAreaSizeChooser.Value + ")";
        }

        internal int getEdgeCount()
        {
            if ( this.triangleRadioButton.Checked)
            {
                return 3;
            }

            return 4;
        }

        internal int getMinArea()
        {
            return this.minAreaSizeChooser.Value;
        }

        internal int getAngleTolerance()
        {
            return this.angleToleranceChooser.Value;
        }

        internal int getEdgeAccuracy()
        {
            return this.edgeAccuaryChooser.Value;
        }

        internal int getCannyLinking()
        {
            return this.cannyLinkingChooser.Value;
        }

        internal int getCannyThresh()
        {
            return this.cannyThreshChooser.Value;
        }

        private void cannyThreshChooser_Scroll(object sender, EventArgs e)
        {
            this.cannyThreshLabel.Text = "Canny Thresh (" + this.cannyThreshChooser.Value + ")";
        }

        private void cannyLinkingChooser_Scroll(object sender, EventArgs e)
        {
            this.cannyLinkingLabel.Text = "Canny Linking (" + this.cannyLinkingChooser.Value + ")";
        }

        internal void setEdgeCount(int edgeCount)
        {
            if ( 3 == edgeCount )
            {
                this.triangleRadioButton.Checked = true;
            }
            else
            {
                this.rectangleRadioButton.Checked = true;
            }
        }

        internal void setMinArea(int minArea)
        {
            this.minAreaSizeChooser.Value = minArea;
            minAreaSizeChooser_Scroll(null, null);
        }

        internal void setAngleTolerance(int angleTolerance)
        {
            this.angleToleranceChooser.Value = angleTolerance;
            angleToleranceChooser_Scroll(null, null);
        }

        internal void setEdgeAccuracy(int accuarcy)
        {
            this.edgeAccuaryChooser.Value = accuarcy;
            edgeAccuaryChooser_Scroll(null, null);
        }

        internal void setCannyThresh(int cannyThresh)
        {
            this.cannyThreshChooser.Value = cannyThresh;
            cannyThreshChooser_Scroll(null, null);
        }

        internal void setCannyLinking(int cannyLinking)
        {
            this.cannyLinkingChooser.Value = cannyLinking;
            cannyLinkingChooser_Scroll(null, null);
        }

        internal void setTimeLeftLabel(string p)
        {
            timeLeftLabel.Text = p;
        }

        internal void enableStartButton()
        {
            startButton.Enabled = true;
        }

        internal Button getStartButton()
        {
            return startButton;
        }
    }
}
