﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.ImageProcessing
{
    interface IFrameInfo
    {
        long frameNumber { get; set; }

        List<IFrameObject> frameObjects { get; set; }
    }
}
