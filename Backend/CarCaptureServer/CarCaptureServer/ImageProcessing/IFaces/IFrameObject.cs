﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CarCaptureServer.ImageProcessing
{
    interface IFrameObject
    {
        // player has id 0
        int ID { get; set; }
        PointF pos { get; set; }
        PointF vel { get; set; }
        int size { get; set; }

        bool aiControlled { get; set; }

        long lastFrame { get; set; }
        long firstFrame { get; set; }

        float distanceTo(IFrameObject other);
    }
}
