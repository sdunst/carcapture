﻿using CarCaptureServer.Networking;
using CarCaptureServer.Networking.SubCommands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CarCaptureServer.CarControl
{
    class CarController
    {
        private UDPServer server;
        private IPAddress ip;

        public event ShootEvent ShotFired;
        public delegate bool ShootEvent();

        private Timer keepAlive;
        private const int KEEP_ALIVE = 400;

        internal CarController(string ip)
        {
            this.ip = IPAddress.Parse(ip);
            this.keepAlive = new Timer(KEEP_ALIVE);
        }

        void keepAlive_Elapsed(object sender, ElapsedEventArgs e)
        {
            server.SendMessage(KeepAliveCmd.CreateNew(), ip);
        }

        internal void SendCommand(ControlCarCmd command, IPAddress senderIp = null)
        {
            if (command.Method == ControlCarCmd.ControlMethod.Drive)
            {
                server.SendMessage(Command.CreateNew(CommandCode.ControlCar, command), ip);
            }
            else if (command.Method == ControlCarCmd.ControlMethod.Shoot)
            {
                bool result = ShotFired != null && ShotFired();
                server.SendMessage(ShootResultCmd.CreateShootResultCommand(result), senderIp);
            }
        }

        internal void Init(UDPServer server)
        {
            this.server = server;
            keepAlive.AutoReset = true;
            keepAlive.Elapsed += keepAlive_Elapsed;
            keepAlive.Start();
        }
    }
}
