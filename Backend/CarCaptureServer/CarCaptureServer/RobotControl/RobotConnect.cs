﻿using CarCaptureServer.Networking.SubCommands;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CarCaptureServer.RobotControl
{
    public class RobotConnect
    {
        private UdpClient client;

        private Timer keepAlive;
        private const int KEEP_ALIVE = 400;

        private string ip;

        public RobotConnect(int port)
        {
            this.ip = ConfigurationManager.AppSettings["robotBoneIP"];
            client = new UdpClient(ip, port);

            keepAlive = new Timer(KEEP_ALIVE);
            keepAlive.AutoReset = true;
            keepAlive.Elapsed += keepAlive_Elapsed;
            keepAlive.Start();
        }

        void keepAlive_Elapsed(object sender, ElapsedEventArgs e)
        {
            var bytes = KeepAliveCmd.CreateNew().GetBytes().Select(x => (byte)x).ToArray();
            client.Send(bytes, bytes.Length);
        }

        public void Control(sbyte carNr, CarCaptureServer.RobotControl.ControlEnums.Direction dir,
            CarCaptureServer.RobotControl.ControlEnums.Steering steering)
        {
            sbyte[] packet = new sbyte[3];
            packet[0] = carNr;
            packet[1] = (sbyte)dir;
            packet[2] = (sbyte)steering;

            client.Send(packet.Select(x => (byte)x).ToArray(), packet.Length);
        }
    }
}
