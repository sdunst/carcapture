﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.RobotControl
{
    public class ControlEnums
    {
        public enum Direction : sbyte
        {
            None = 0,
            Backwards = -1,
            Forwards = 1
        }

        public enum Steering : sbyte
        {
            Straight = 0,
            Left = -1,
            Right = 1
        }
    }
}
