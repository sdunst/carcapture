﻿using CarCaptureServer.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Routing
{
    abstract class Routing : IRouting
    {
        public RoutingGroup RoutingGroup { get; set; }

        public Routing(RoutingGroup routingGroup)
        {
            RoutingGroup = routingGroup;
        }

        public abstract Move Route();

        protected static PointF add(PointF p1, PointF p2)
        {
            return new PointF(p1.X + p2.X, p1.Y + p2.Y);
        }

        protected static PointF subtract(PointF p1, PointF p2)
        {
            return new PointF(p1.X - p2.X, p1.Y - p2.Y);
        }

        protected static double dotProduct(PointF p1, PointF p2)
        {
            return p1.X * p2.X + p1.Y * p2.Y;
        }

        protected static double length(PointF p)
        {
            return Math.Sqrt(p.X * p.X + p.Y * p.Y);
        }
    }
}
