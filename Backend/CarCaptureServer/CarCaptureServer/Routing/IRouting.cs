﻿using CarCaptureServer.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Routing
{
    interface IRouting
    {
        Move Route();
    }
}
