﻿using CarCaptureServer.RobotControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Routing
{
    class Move
    {
        public int ID { get; set; }
        public ControlEnums.Direction Direction { get; set; }
        public ControlEnums.Steering Steering { get; set; }
    }
}
