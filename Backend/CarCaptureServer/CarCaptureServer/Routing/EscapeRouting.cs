﻿using CarCaptureServer.ImageProcessing;
using CarCaptureServer.RobotControl;
using CarCaptureServer.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Routing
{
    class EscapeRouting : Routing
    {
        private IFrameObject car;

        private ControlEnums.Steering steering;

        public EscapeRouting(RoutingGroup routingGroup, IFrameObject car) : base(routingGroup)
        {
            this.car = car;
        }

        public override Move Route()
        {
            Move move = new Move();

            move.ID = car.ID;

            if (RoutingGroup.FrameInfo.frameNumber % 4 == 0)
            {
                move.Direction = ControlEnums.Direction.None;
            }
            else
            {
                move.Direction = ControlEnums.Direction.Forwards;
            }

            Console.WriteLine("Position:" + this.car.pos);
            Console.WriteLine("Velocity:" + this.car.vel);

            String direction = null;
            float min = 640;

            float distLeft = this.car.pos.X;
            float distTop = this.car.pos.Y;
            float distRight = 640 - this.car.pos.X;
            float distBottom = 480 - this.car.pos.Y;

            if (distLeft < min)
            {
                direction = "left";
                min = distLeft;
            }
            if (distTop < min)
            {
                direction = "top";
                min = distTop;
            }
            if (distRight < min)
            {
                direction = "right";
                min = distRight;
            }
            if (distBottom < min)
            {
                direction = "bottom";
                min = distBottom;
            }
            
            if (min < 150)
            {
                switch (direction)
                {
                    case "left":
                        CenterLeft();
                        break;
                    case "top":
                        CenterTop();
                        break;
                    case "right":
                        CenterRight();
                        break;
                    case "bottom":
                        CenterBottom();
                        break;
                }
            }
            else
            {
                Escape();
            }

            if (RoutingGroup.FrameInfo.frameNumber % 20 == 0)
            {
                if (this.car.pos.X < 150)
                {
                    CenterLeft();
                }
                else if (this.car.pos.Y < 150)
                {
                    CenterTop();
                }
                else if (this.car.pos.X > 460)
                {
                    CenterRight();
                }
                else if (this.car.pos.Y > 300)
                {
                    CenterBottom();
                }
                else
                {
                    Escape();
                }
            }

            move.Steering = steering;

            return move;
        }

        private void CenterBottom()
        {
            if (this.car.vel.Y < 0)
            {
                steering = ControlEnums.Steering.Straight;
            }
            else
            {
                steering = ControlEnums.Steering.Right;
            }
        }

        private void CenterRight()
        {
            if (this.car.pos.X < 0)
            {
                steering = ControlEnums.Steering.Straight;
            }
            else
            {
                steering = ControlEnums.Steering.Right;
            }
        }

        private void CenterTop()
        {
            if (this.car.vel.Y > 0)
            {
                steering = ControlEnums.Steering.Straight;
            }
            else
            {
                steering = ControlEnums.Steering.Right;
            }
        }

        private void CenterLeft()
        {
            if (this.car.vel.X > 0)
            {
                steering = ControlEnums.Steering.Straight;
            }
            else
            {
                steering = ControlEnums.Steering.Right;
            }
        }

        private void Escape()
        {
            PointF aiToPlayer = subtract(RoutingGroup.Player.pos, car.pos);

            PointF playerVelocity = length(RoutingGroup.Player.vel) == 0 ? aiToPlayer : RoutingGroup.Player.vel;
            PointF carVelocity = length(car.vel) == 0 ? new PointF(1, 1) : car.vel;

            // Add turned player velocity to avoid collision with player
            PointF direction = add(aiToPlayer, new PointF(-playerVelocity.X, playerVelocity.Y));
            double product = dotProduct(direction, carVelocity);

            // Calculate direction
            double angle = (180 / Math.PI) * Math.Acos(product / (length(direction) * length(carVelocity)));

            if (angle > 5)
            {
                if (product > 0)
                {
                    steering = ControlEnums.Steering.Left;
                }
                else
                {
                    steering = ControlEnums.Steering.Right;
                }
            }
        }
    }
}
