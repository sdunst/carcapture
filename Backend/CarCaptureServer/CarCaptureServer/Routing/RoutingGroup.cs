﻿using CarCaptureServer.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarCaptureServer.Routing
{
    class RoutingGroup
    {
        private Dictionary<int, IRouting> routings = new Dictionary<int, IRouting>();

        public IFrameObject Player { get; set; }

        public IFrameInfo FrameInfo;

        public RoutingGroup(IFrameInfo info)
        {
            FrameInfo = info;
            
            foreach (IFrameObject frameObject in info.frameObjects)
            {
                if (frameObject.aiControlled)
                {
                    routings.Add(frameObject.ID, new EscapeRouting(this, frameObject));
                }
                else
                {
                    Player = frameObject;
                }
            }
        }

        public List<Move> Route(IFrameInfo info)
        {
            FrameInfo = info;
            List<Move> moves = new List<Move>();

            SortedSet<IFrameObject> order = new SortedSet<IFrameObject>(new FrameObjectComparer(info, Player.ID));
            foreach (KeyValuePair<int, IRouting> entry in routings)
            {
                order.Add(info.frameObjects[entry.Key]);
            }

            // Sort routings ascending by distance to player first
            foreach (IFrameObject entry in order)
            {
                moves.Add(routings[entry.ID].Route());
            }

            return moves;
        }

        class FrameObjectComparer : IComparer<IFrameObject>
        {
            private IFrameInfo info;

            private int player;

            public FrameObjectComparer(IFrameInfo info, int player)
            {
                this.info = info;
                this.player = player;
            }

            public int Compare(IFrameObject x, IFrameObject y)
            {
                float distX = x.distanceTo(info.frameObjects[player]);
                float distY = y.distanceTo(info.frameObjects[player]);

                return Convert.ToInt32(Math.Round(distX - distY));
            }
        }
    }
}
