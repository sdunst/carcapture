﻿using CarCaptureServer.CarControl;
using CarCaptureServer.ImageProcessing;
using CarCaptureServer.Networking;
using CarCaptureServer.VideoStream;
using CarCaptureServer.RobotControl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using CarCaptureServer.Routing;

namespace CarCaptureServer
{
    class Program
    {
        private static float MAX_HIT_DISTANCE = 10;

        private static RoutingGroup routingGroup;
        private static IFrameObject player;
        private static IFrameObject ai;
        private static RobotConnect rc;
        private static PositionDetection positionDetection;

        private static int score;

        private static CarCaptureServer.RobotControl.ControlEnums.Direction previousDir;
        private static CarCaptureServer.RobotControl.ControlEnums.Steering previousSteer;

        static void Main(string[] args)
        {
            Program.rc = new RobotConnect(4713);
            Program.rc.Control(2, CarCaptureServer.RobotControl.ControlEnums.Direction.None, CarCaptureServer.RobotControl.ControlEnums.Steering.Straight);
            Program.rc.Control(3, CarCaptureServer.RobotControl.ControlEnums.Direction.None, CarCaptureServer.RobotControl.ControlEnums.Steering.Straight);
            
            CarController carController = new CarController(ConfigurationManager.AppSettings["CarIP"]);           
            VideoStreamer videoStreamer = new VideoStreamer();

            carController.ShotFired += carController_ShotFired;

            positionDetection = new PositionDetection();
            positionDetection.ObjectDetected += positionDetection_ObjectDetected;
            positionDetection.ObjectLost += positionDetection_ObjectLost;
            positionDetection.FrameUpdated += positionDetection_FrameUpdated;
            positionDetection.Calibrated += positionDetection_Calibrated;
            positionDetection.GameStarted += positionDetection_GameStarted;
            positionDetection.GameOver += positionDetection_GameOver;
            score = 0;

            UDPServer server = new UDPServer(carController, positionDetection, videoStreamer);
            carController.Init(server);
            server.Start();

            while (true)
            {

            }
        }

        static void positionDetection_GameOver()
        {
            // when game started: ai-car stops
            Program.rc.Control(2, ControlEnums.Direction.None, ControlEnums.Steering.Straight );
        }

        static void positionDetection_GameStarted()
        {
            previousDir = ControlEnums.Direction.Forwards;
            previousSteer = ControlEnums.Steering.Straight;

            // when game started: ai-car steers forward straight
            Program.rc.Control(2, previousDir, previousSteer);
        }

        static bool carController_ShotFired()
        {
            PointF aiToPlayerVec = new PointF(player.pos.X - ai.pos.X, player.pos.Y - ai.pos.Y);
            double vecLen = Math.Sqrt(aiToPlayerVec.X * aiToPlayerVec.X + aiToPlayerVec.Y * aiToPlayerVec.Y);

            if (vecLen - player.size - ai.size <= MAX_HIT_DISTANCE)
            {
                positionDetection.shotHit(++score);

                return true;
            }

            return false;
        }

        static void positionDetection_Calibrated(IFrameInfo info)
        {
            routingGroup = new RoutingGroup(info);

            foreach (IFrameObject fo in info.frameObjects)
            {
                if ( fo.aiControlled )
                {
                    ai = fo;
                    
                }
                else
                {
                    player = fo;
                }
            }
        }

        static void positionDetection_FrameUpdated(IFrameInfo info)
        {
            List<Move> moves = routingGroup.Route(info);

            foreach (Move move in moves)
            {
                Program.rc.Control((sbyte) (move.ID + 1), move.Direction, move.Steering);
            }

            /*
            // carNr starts with 2, ai-controlled id with 1 => +1
            sbyte carNr = 2;

            if (previousDir == ControlEnums.Direction.None && 0 == info.frameNumber % 30)
            {
                previousDir = ControlEnums.Direction.Forwards;
                previousSteer = ControlEnums.Steering.Straight;

                PointF aiToPlayerVec = new PointF(player.pos.X - ai.pos.X, player.pos.Y - ai.pos.Y);
                double vecLen = Math.Sqrt(aiToPlayerVec.X * aiToPlayerVec.X + aiToPlayerVec.Y * aiToPlayerVec.Y);
                double velocityLen = Math.Sqrt(ai.vel.X * ai.vel.X + ai.vel.Y * ai.vel.Y);

                double dotProduct = aiToPlayerVec.X * ai.vel.X + aiToPlayerVec.Y * ai.vel.Y;
                double angle = (180.0 / Math.PI) * Math.Acos(dotProduct / (vecLen * velocityLen));

                if (angle > 10.0 || angle < 350.0 )
                {
                    if (0 >= dotProduct)
                    {
                        previousSteer = ControlEnums.Steering.Right;
                    }
                    else
                    {
                        previousSteer = ControlEnums.Steering.Left;
                    }
                }

                if (previousSteer == ControlEnums.Steering.Right)
                {
                    Console.WriteLine("Right Forward");
                }
                else
                {
                    Console.WriteLine("Left Forward");
                }

                Program.rc.Control(carNr, previousDir, previousSteer);
            }
            else if (previousDir == ControlEnums.Direction.Forwards && 0 == info.frameNumber % 10)
            {
                previousDir = ControlEnums.Direction.None;

                if (previousSteer == ControlEnums.Steering.Right)
                {
                    Console.WriteLine("Right None");
                }
                else
                {
                    Console.WriteLine("Left None");
                }
				
				Program.rc.Control(carNr, previousDir, previousSteer);
			}
             */
        }

        static void printFrameInfo(IFrameInfo info)
        {
            /*
            if (0 != info.frameObjects.Count)
            {
                Console.WriteLine("FrameUpdate# " + info.frameNumber);

                foreach (IFrameObject fo in info.frameObjects)
                {
                    Console.WriteLine("    FrameObject ID: " + fo.ID);
                    Console.WriteLine("        Velocity: " + fo.vel);
                    Console.WriteLine("        Position:  " + fo.pos);
                    Console.WriteLine("        AI:  " + fo.aiControlled);
                }
            }
             */
        }

        static void positionDetection_ObjectLost(IFrameObject info)
        {
            Console.WriteLine("Object Lost: " + info.ID);
        }

        static void positionDetection_ObjectDetected(IFrameObject info)
        {
            Console.WriteLine("Object Detected: " + info.ID);
        }
    }
}
