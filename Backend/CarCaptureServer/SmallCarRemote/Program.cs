﻿using CarCaptureServer.RobotControl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallCarRemote
{
    class Program
    {
        static void Main(string[] args)
        {
            RobotConnect rc = new RobotConnect(4713);

            string inputDir = "";
            string inputSteer = "";

            do
            {
                sbyte carNr = sbyte.Parse(Console.ReadLine());
                inputDir = Console.ReadLine();
                inputSteer = Console.ReadLine();

                CarCaptureServer.RobotControl.ControlEnums.Direction dir;
                CarCaptureServer.RobotControl.ControlEnums.Steering steer;

                switch(inputDir)
                {
                    case "f":
                        dir = ControlEnums.Direction.Forwards;
                        break;
                    case "b":
                        dir = ControlEnums.Direction.Backwards;
                        break;
                    default:
                        dir = ControlEnums.Direction.None;
                        break;
                }

                switch (inputSteer)
                {
                    case "l":
                        steer = ControlEnums.Steering.Left;
                        break;
                    case "r":
                        steer = ControlEnums.Steering.Right;
                        break;
                    default:
                        steer = ControlEnums.Steering.Straight;
                        break;
                }

                rc.Control(carNr, dir, steer);

            } while (inputDir != "q");
        }
    }
}
