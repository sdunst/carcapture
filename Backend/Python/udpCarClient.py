import socket
import struct

soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def drive_left(speed):
	print("left, speed:" + str(speed))
	
def drive_right(speed):
	print("right, speed:" + str(speed))

def drive_straight(speed):
	if(speed > 127):
		print("backwards, speed:" + str(speed-127))
	else:
		print("straight, speed: " + str(speed))
	
	
def shoot():
	print("shoot")
	
def work_rec_data(data):
	d_or_s = int(data[1].encode('hex'),16)
	speed = 0
	dir = 0
	if(d_or_s == 1):
		speed = int(data[2].encode('hex'),16)
		dir = int(data[3].encode('hex'),16)
		if(dir == 0):
			drive_straight(speed)
		elif(dir <= 127):
			drive_right(speed)
		elif(dir > 127):
			drive_left(speed)
	elif(d_or_s == 2):
		shoot()

def con_udp(server_port):
	soc.bind(("0.0.0.0", int(server_port)))#sendToAll
	
	exit = raw_input("")
	while(exit != "exit"):
		data, addr = soc.recvfrom(1024)
		work_rec_data(data)
	soc.close()

def connect(serverip):
	if(serverip == "D"):
		serverip = "172.16.50.225"
	server_port = 4711

def main():
	try:
		con_udp(4711)
	except:
		soc.close()
		
if __name__ == '__main__':
		main()
